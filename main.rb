require_relative 'ResponserHttp.rb'
require_relative 'Circle.rb'
require_relative 'Points.rb'
require_relative 'Error.rb'

include ResponserHttp

API_KEY_STRING = "cRuPdopsLI6U8jlF+njwIZFB9lSXAn8ryk2QlpXyYULIPkBExL9gWayvdA4DhLpwXsm9HFCm2R0Z/V6RqdkSQw=="


def tryCorrectError()
  errorRequest = ResponserHttp.getRequest()
  isCorrectResponseError = isCorrectResponse(errorRequest)
  if(!isCorrectResponseError)
    error = getErrorMessage(errorRequest)
    puts("#{error.message} code:#{error.code}")
  end
end

def main()
  arrayCircles=[]
  jsonArray = ResponserHttp.getTask(API_KEY_STRING)
  isCorrectResponse = isCorrectResponse(jsonArray)
  if(isCorrectResponse)
    arrayJsonClass = parseJson(jsonArray)
    for i in 0..arrayJsonClass.length() - 1
      arrayCircles[i] = getRectangleCoordinate(arrayJsonClass[i])
    end
    tryCorrectCoordinateRect = checkResult(arrayCircles)
    isCorrectResponse = isCorrectResponse(tryCorrectCoordinateRect)
    if(isCorrectResponse)
      getCorrectResultCoordinate(tryCorrectCoordinateRect)
    else
      error = getErrorMessage(jsonArray)
      puts("#{error.message} code:#{error.code}")
    end
  else
    error = getErrorMessage(jsonArray)
    puts("#{error.message} code:#{error.code}")
  end
end


def isCorrectResponse(res)
  data = JSON.parse(res)
  error = data['error']!=nil
  return !error
end

def getErrorMessage(res)
  data = JSON.parse(res)
  errorJson = data['error']
  error = Error.new(errorJson['code'],errorJson['message'])
  return error
end

def  parseJson (res)
  data = JSON.parse(res)
  len =  data['result'].each { |x|  arr = Array.new()
    a = x.map {|circle| Circle.new(circle['radius'],circle['x'],circle['y'])}
  arr.push(a)}
  return len
end

def getRectangleCoordinate(data)
   minX = data[0]['x']
   minY = data[0]['y']
   maxX = data[0]['x']
   maxY = data[0]['y']

 for i in 0..data.length() - 1
   minXi = data[i]['x'] - data[i]['radius']
   minYi = data[i]['y'] - data[i]['radius']
   maxXi = data[i]['x'] + data[i]['radius']
   maxYi = data[i]['y'] + data[i]['radius']
   if(minXi < minX)
     minX = minXi
   end
   if(minYi < minY)
     minY = minYi
   end
   if(maxXi > maxX)
     maxX = maxXi
   end
   if(maxYi > maxY)
     maxY = maxYi
   end
 end
   left_bottom = Point.new(minX,minY)
   right_top = Point.new(maxX,maxY)
   rect = PointsRectangle.new(left_bottom,right_top)
  return rect
end

def checkResult(arrayJson)
  res = ResponserHttp.checkResults(API_KEY_STRING,arrayJson)
  return res
end
def getCorrectResultCoordinate(res)
    data = JSON.parse(res)
    elemsOfTry = data['result']
    puts(elemsOfTry.to_s)
end
# tryCorrectError()
main()



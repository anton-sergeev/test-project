class Point
  attr_accessor :x,:y
  def initialize(x,y)
    @x = x
    @y = y
  end
  # def to_json
  #   {'x' => @x,
  #    'y' => @y
  #   }.to_json
  # end
end
class PointsRectangle
  attr_accessor :left_bottom,:right_top
  def initialize(left_bottom,right_top)
    @left_bottom = left_bottom
    @right_top = right_top
  end
  # def to_json
  #   {'left_bottom' =>{
  #       'x'=>@left_bottom.x,
  #       'y'=>@left_bottom.y
  #   },
  #    'right_top' =>{
  #        'x'=>@right_top.x,
  #        'y'=>@right_top.y
  #    }
  #   }.to_json
  # end
end
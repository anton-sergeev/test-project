require 'net/http'
require 'json'
require 'uri'

module  ResponserHttp

  def getTask(key)
    data = {'key'=>key,
            'method'=>"GetTasks",
            'params' => nil}
    res = postRequest(data)
    return res
  end

  def checkResults(key,params)
    data = {'key' => key,
            'method' => "CheckResults",
            'params' => params.map { |item|  {"left_bottom" =>{"x"  => item.left_bottom.x,
                                                               "y"  => item.left_bottom.y},
                                              "right_top" =>{"x"  => item.right_top.x,
                                                             "y"  => item.right_top.y},}}}
    res = postRequest(data)
    return res
  end

  def postRequest(data)
    uri = URI.parse('http://contest.elecard.ru/api')
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Post.new(uri.request_uri)
    request.body = data.to_json
    request.content_type='application/json'
    response = http.request(request)
    return response.body
  end
  def getRequest()
    uri = URI.parse('http://contest.elecard.ru/api')
    req = Net::HTTP::Get.new(uri.to_s)
    res = Net::HTTP.start(uri.host, uri.port) {|http| http.request(req)}
    return res.body
  end
end
